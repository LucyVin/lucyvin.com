---
title: "Safer Better"
date: 2021-05-20T16:01:33-04:00
slug: safer-better
draft: false
tags: ["anarchism", "cycling", "creative writing"]
categories: ["Cycling", "Creative Writing"]
author: "Lucy Vin"
comment: false
toc: false
---
I once got a message in my inbox that read, “one day in a safer better world, let’s go on a really awesome bike ride together.” I think about it a lot. The person who sent it isn’t someone I’m distinctly close with - at least, not beyond the limits that a social media platform imposes - but that doesn’t matter. What would it be like to go on a bike ride in a safer better world? Even to my optimistic mind, it feels impossible to picture. 

In a safer better world, I could bike anywhere, unfettered by borders or infrastructure. In a safer better world I would not need to carry 50 pounds of locks to do my grocery shopping, because economically motivated “crime” would not be a thing. We would be cared for, trusted with our own bodies, and welcome all over without worry of persecution for our identities or sexuality. Rather than sitting at my work-issued laptop, doing profit-motivated labor, we could spend our time building, creating, and exploring without fear of losing a home, being unable to eat, or not being able to afford a doctor’s visit. 

In a safer better world, people would wear masks when they’re told they need to, they’d care about those around them, and they’d step up for their neighbors when they’re in need. 

In a safer better world, cycling wouldn’t be seen as the sport of middle-aged men with disposable income. Cycling to commute wouldn’t come with a look of confused concern. “Don’t you have a car? They don’t cost that much money”. 

In a safer better world, the streets are designed for people. We would never need to read about another crash caused by an inattentive driver. We would never need to worry about close-passes, brake checks, and car doors.

In a safer better world, bike lanes are separated from traffic, cars drive at reasonable speeds, and there’s plenty of places to lock your bike.

In a safer better world, things are done for people, not machines.

One day in a safer better world, let’s go on a really awesome bike ride together. 

---

I'll admit that sometimes I feel like my creative writing reads like high school english class material. But I've been thinking about this non-stop since the message was sent to me and figure there's no reason I can't post it online. Stay safe and enjoy your rides, y'all.

\- Lucy Vin
