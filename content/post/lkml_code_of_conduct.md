---
title: LKML 4.19 RC4 and Code of Conduct tantrums
slug: lkml-419-rc4-coc
date: 2018-09-17T12:00:00-05:00
draft: false
tags: ["linux", "code of conduct", "linus torvalds"]
categories: ["linux"]
author: "Lucy Vin"
comment: false
toc: false
---
**Yesterday, on September 16th,** [Linus Torvalds emailed the Linux Kernal Mailing List](https://lkml.org/lkml/2018/9/16/167) regarding Kernel Patch 4.19. The email starts off like any other, reviewing how the patch has progressed, going over any delays, and discussing what has been added or updated within the Kernel. Then, he addressed a change which was somewhat unusual: the removal of the Code of Criticism, and addition of a Code of Conduct. 

Linus wrote, regarding his personal conduct and recent behavior surrounding the Kernel Summit:
 
>This is my reality.  I am not an emotionally empathetic kind of person and that probably doesn't come as a big surprise to anybody.  Least of all me.  The fact that I then misread people and don't realize (for years) how badly I've judged a situation and contributed to an unprofessional environment is not good.
>
>This week people in our community confronted me about my lifetime of not understanding emotions.  My flippant attacks in emails have been both unprofessional and uncalled for.  Especially at times when I made it personal.  In my quest for a better patch, this made sense to me. I know now this was not OK and I am truly sorry.
>
>The above is basically a long-winded way to get to the somewhat painful personal admission that hey, I need to change some of my behavior, and I want to apologize to the people that my personal behavior hurt and possibly drove away from kernel development entirely.

This statement hits on a point that I, along with many other individuals I've spoken with regarding Linux and the F/OSS movement have repeated for awhile now: the leadership's behavior is frequently abrasive. Linus along with several other individuals in leadership positions have long had a tendency towards antagonism, even when giving legitimate and valuable criticism. This lack of consideration and care towards the words they use trickles out into the community at large. It poisons the well. I have seen it drive many people away from working on F/OSS projects, time and time again. 

On a grand scale, one of the biggest pain points I often hear in regards to interacting with tech communities is that members often have a tendency towards blunt, occasionally hostile dismissal of those who challenge the status quo, be it via RFC, behavior, or even their identity. Yes, there *are* good technical communities. The existance of good communities doesn't erase the difficulty of working with the vast pool of less than pleasant ones. It's a detriment to productivity, if nothing else, if workers do not feel comfortable doing their work. It reflects poorly on the movement, it drives people away from the cause of Libre software. A cause which I believe is invaluable and necessary.

Linus goes on:
>Put another way: When asked at conferences, I occasionally talk about how the pain-points in kernel development have generally not been about the _technical_ issues, but about the inflection points where development flow and behavior changed.
>
>These pain points have been about managing the flow of patches, and often been associated with big tooling changes - moving from making releases with "patches and tar-balls" (and the _very_ painful discussions about how "Linus doesn't scale" back 15+ years ago) to using BitKeeper, and then to having to write git in order to get past the point of that no longer working for us.
>
>We haven't had that kind of pain-point in about a decade.  But this week felt like that kind of pain point to me.

This is an encouraging thing to see from Linus. Making a concerted effort to improve experience of participation in Kernel development is massively important. Linus' admission of culpability, whether you read it as genuine or as a response to some coersion - as I have seen some suggest - sets a precedent within the leadership that acknowledges this toxicity, and sets forth a stated desire to improve upon this "pain point". 

Sadly, the topic doesn't end there. As with any code of conduct update, a contingent of vocal opponents of the change have taken to decrying the email, as well as [the new Code of Conduct](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=8a104f8b5867c682d994ffa7a74093c54469c11f), as somehow insidious. The less tactful complaints range the gamut from "SJWs are ruining Linux" to "F\*\*\*k your feelings". The more delicately worded statements generally reflect one or both of the following two points:

1. I don't want politics in my development spaces
1. I don't want speech policed

There are fundemental problems with both of these responses. In the case of the first point, "I don't want politics in my development space", the argument posits that a space can exist without any personal politics being involved. This is fallacious on premise alone, as any interactions between two individuals inherently introduces some aspect of personal politics. It may be the case that, in a good majority of interactions, individuals' personal politics are not in direct conflict. As such these issues can often be ignored until the point that problems arise, should it happen. Conflict or not, personal politics *are* still in play in these scenarios. Giving critique and response without attacking a person's self is often harder than it sounds, but it is a pursuit worth putting effort towards, in order to minimize these "political conflicts".

To the second point, the notion that speech being regulated within an organizational space is somehow detrimental presents a philosophical dillema. I'd like to state, as point of context, that I am in no way a believer in free speech absolutism. I think there is a necessity to regulate speech, as speech does not exist in a vacuum. The effects of speech on an individual or a group of individuals are very real, particularly when it comes to incitement of action.

That out of the way, I am troubled by the notion that poor behavior having consequences in a formal or semi-formal setting such as a development space should be viewed as a detriment. This is exactly what you would expect of an office environment, why would you expect anything less in a space with similar levels of formality? A public software project, especially one on the scale of Linux, must by its nature adhere to such a degree of professionalism. Straying from that ethos would create issues of approachability for a project. Linux would absolutley not be where it is today were it not for the massive professional backing it has. A large portion of that growth can only be attributed to the formal face of the project. This is evidenced by Linus' own admission that one of the largest pain points for individuals has been not technical, but interpersonal. 

Further, the implication that the fault of offense is by default on the offense taker belies a disturbing desire to absolve guilt from one's self. The argument always goes, "well if someone takes offense at a criticism of their code, I don't want to get banned". Some form of this appears in just about every conversation about regulations of behavior and speech I have seen or participated in. It is a deceptive statement that discards the very fundamental building blocks of the regulations themselves. It ignores the structure of the system, so that it can posit a hypothetical scenario that feels dangerous to the listener, despite the fact that said hypothetical is *not* in offense of the CoC -- given the criticism did not resort to personal attack, and was not unjustly provided based on the individuals' identity. It should be possible to make criticisms of an individual's work without making attacks at their person. If that individual feels offended that their code is rejected on the basis of the code's merits, that's fine. But if the criticizer uses, say, poor code as a vector to make directed attacks at that individual's person, then that *is* an issue, and it should be addressed. If the actions warrant a ban, then so be it. However, these things are rarely so cut and dry as to be decided and actioned immediately, much less with "corporal" punishment. I've seen plenty of people break rules in communities only to be issued warnings, occaisionally for quite large infractions.

We do not exist in a vacuum. Actions have reactions, and we must be conscious of those reactions when we are participating in these communities. It should not present a major point of contention for there to be an expectation of cooperation and consideration when acting within these spaces. I find the recent developments a welcome change of pace. I can only hope more projects follow suit, and that the F/OSS development space continues to progress into a positive community for all developers. 

Be excellent to each other.

*- Lucy Vin*
