---
title: "Warped World (an SCP Roleplaying game) Preview"
slug: "warped-world-preview"
date: 2021-06-05T19:51:57-04:00
draft: false
tags: ["tabletop", "dungeon world", "warped world"]
categories: ["tabletop"]
author: "Lucy Vin"
comment: false
toc: false
---
I've spent the past month or three working on a pet project I'd been mulling over for years. I have to admit, I am a huge fan of the SCP-wiki and all of its various tales, anomalies, and groups of interest. As a piece of collaborative emergent fiction, it's absolutely amazing to me. I particularly find myself enamored by the numerous canons woven together, from cyberpunk dystopia, to cultic nightmare apocalypse. Naturally, I've wanted to play a TTRPG that works in that setting for about as long as I've been reading it. Unfortunately, to my knowledge, nothing so broad exists. There are countless homebrews and supplements to support the core concept - that of an MTF agent or foundation researcher trying to chase down and contain stray anomalies. Beyond that, however, you're on your own. Additionally, the very fact that SCP's "lore" is fluid and self-contradicting, it's actually fairly difficult to write a ruleset that doesn't inherently canonize one branch over another. For my part, I don't know that I've truly solved this, but I do think I've done as much due dilligence as I possibly could to avoid setting a strict timeline or ruleset to the universe. I've chosed Dungeon World as a base for this inital playtest, given its more dynamic and flexible structure and its propensity towards archetypal playbooks, not strict classes. There's every possibility that this morphs into its own Powered by the Apocalypse game, or a full Dungeon World remix, but for now we're gonna stick with a supplement for Dungeon World. 

This is a **very early** release, and needs a lot of tweaking and testing before it's fully ready. What you see here is a logical stopping point, a set of tools to run a short campaign up to level 5. This should give me an opportunity to adjust, refactor, and add to the existing playbooks. I also hope to add more playbooks to the mix, including a meta-playbook to cover translations from SCP listings themselves, where possible. As of right now, the following exists in the early preview:

- 5 Playbooks
  - Researcher
  - Anartist
  - Reality Bender
  - Thaumatologist
  - Field Agent
- 3 optional additional moves
  - Tinkering
  - Maintain a Move
  - Deceive
- A short glossary to clear up some jargon

If you do playtest it, or even if you don't and you just have some notes, please feel free to send them to me at [lucyvin@pm.me](mailto:lucyvin@pm.me).

[Click here to download the Warped World preview](/files/warped_world_early_playtest_ed.pdf)
